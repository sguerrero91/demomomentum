//
//  GlobalVars.swift
//  TestMomentum
//
//  Created by Sebastian Guerrero on 5/15/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import Foundation
import UIKit

var productArray = [Product]() {
    
    didSet{
        
        NotificationCenter.default.post(name: NSNotification.Name("reloadTVData"), object: nil, userInfo: nil)
        NotificationCenter.default.post(name: NSNotification.Name("reloadCVData"), object: nil, userInfo: nil)
        
    }
}

var titleSize:CGFloat = 18 {
    
    didSet {
        
        NotificationCenter.default.post(name: NSNotification.Name("reloadTVData"), object: nil, userInfo: nil)
        NotificationCenter.default.post(name: NSNotification.Name("reloadCVData"), object: nil, userInfo: nil)
        
    }
    
}

var subTitleSize:CGFloat = 17 {
    
    didSet {
        
        NotificationCenter.default.post(name: NSNotification.Name("reloadTVData"), object: nil, userInfo: nil)
        NotificationCenter.default.post(name: NSNotification.Name("reloadCVData"), object: nil, userInfo: nil)
        
    }
    
}

var nextPage:String! // saves the url for the next page
