//
//  ProductsTableViewController.swift
//  TestMomentum
//
//  Created by Sebastian Guerrero on 5/10/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import UIKit
import ReachabilitySwift
import CoreData

class ProductsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {    
    
    
    var selectedIndex = 0
    var activeSearch = false
    
    var searchController: UISearchController!
    var filteredProducts = [Product]()
    var products: [NSManagedObject] = []
    
    var downloadingNextPage = false
    var rowForPagination = 0
    
    let reachability = Reachability(hostname: "www.google.com")!
    
    @IBOutlet weak var productTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NSNotification.Name("reloadTVData"), object: nil)
        
        configSerchBarController()
        
        downloadProducts()
        //backendManager.fetchFromCoreData()
        
        reachability.whenReachable = { reachability in

            DispatchQueue.main.async {
                
                self.downloadProducts()
                
            }
        }
        reachability.whenUnreachable = { reachability in

            
            DispatchQueue.main.async {
                
                backendManager.fetchFromCoreData()
                
            }
        }
        

    }
    

    func downloadProducts(){
        
        backendManager.fetchDataFromAPI(url: "/v1/paginated/items?format=json&apiKey=ru7fqc6pqwcfg7bwrtygckpf") { (nextPageS, products) in
            
            productArray = products
            nextPage = nextPageS

        }
        
    }
    
    
    // MARK: - TableView Settings
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if activeSearch {
            
            return filteredProducts.count
            
        }
        
        return productArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "productTVC") as! ProductTableViewCell
        
        cell.productTitle.font = UIFont(name: cell.productTitle.font.fontName, size: titleSize)
        cell.productSubtitle.font = UIFont(name: cell.productSubtitle.font.fontName, size: subTitleSize)
        
        if activeSearch {
            
            cell.product = filteredProducts[indexPath.row]
            
        } else {
            
            cell.product = productArray[indexPath.row]
            
        }
        
        cell.fillCellData()
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        selectedIndex = indexPath.row
        
        return indexPath
    }

    @IBAction func orderButtonPressed(_ sender: Any) {
        
        let sortedArray = productArray.sorted(by: { $0.name < $1.name })
        productArray = sortedArray
        rowForPagination = 0
        productTableView.reloadData()
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        if !downloadingNextPage && productArray.count > 0 && !activeSearch{
            
            if productTableView.contentOffset.y >= (productTableView.contentSize.height - productTableView.frame.size.height) {
                
                downloadingNextPage = true
                rowForPagination = productArray.count - 1
                
                backendManager.fetchDataFromAPI(url: nextPage) { (nextPageS, products) in
                    
                    productArray += products
                    nextPage = nextPageS
                    self.downloadingNextPage = false
                    
                }
            }
            
        }
        
    }
    
    
    func reloadData(){
        
        downloadingNextPage = false
        productTableView.reloadData()
        productTableView.scrollToRow(at: IndexPath(row: rowForPagination, section: 0), at: .none, animated: true)
        
        
    }
    
    
    //MARK: - Search Bar
    
    func configSerchBarController(){
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        
        productTableView.tableHeaderView = searchController.searchBar
        
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        activeSearch = true
        productTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        activeSearch = false
        productTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if !activeSearch {
            
            activeSearch = true
            productTableView.reloadData()
            
        }
        
        searchController.searchBar.resignFirstResponder()
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        activeSearch = false
        productTableView.reloadData()
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        
        
        filteredProducts = productArray.filter({ (product) -> Bool in
            
            return product.name.lowercased().contains((searchController.searchBar.text?.lowercased())!)
            
        })
        
        productTableView.reloadData()
        
    }
    
    
    //MARK: Prepare for segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destination = segue.destination as! DetailViewController
        
        if activeSearch {
            
            destination.product = filteredProducts[(productTableView.indexPathForSelectedRow?.row)!]
            
        } else {
            
            destination.product = productArray[(productTableView.indexPathForSelectedRow?.row)!]
        }
        
        searchController.isActive = false
 
    }
    

}
