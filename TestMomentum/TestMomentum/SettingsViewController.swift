//
//  SettingsViewController.swift
//  TestMomentum
//
//  Created by Sebastian Guerrero on 5/15/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {

    let sizeOptions = Array(12...25)
    
    @IBOutlet weak var titleSizeTextField: UITextField!
    @IBOutlet weak var subtitleSizeTextField: UITextField!
    var sizePickerView = UIPickerView()
    var textFieldTag = 0
    
    override func viewDidLoad() {

        super.viewDidLoad()

        sizePickerView.delegate = self
        sizePickerView.dataSource = self
        
        titleSizeTextField.inputView = sizePickerView
        subtitleSizeTextField.inputView = sizePickerView
        
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    // MARK: - TextField delegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
     
        textFieldTag = textField.tag
        return true
    }
    
    
    // MARK: - PickerView DataSource & Delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sizeOptions.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(sizeOptions[row])"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch textFieldTag {
        case 1:
            titleSizeTextField.text = "\(sizeOptions[row])"
            titleSize = CGFloat(sizeOptions[row])
        case 2:
            subtitleSizeTextField.text = "\(sizeOptions[row])"
            subTitleSize = CGFloat(sizeOptions[row])
        default:
            return
        }
        
        
    }

}
