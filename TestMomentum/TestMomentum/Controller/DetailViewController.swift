//
//  DetailViewController.swift
//  TestMomentum
//
//  Created by Sebastian Guerrero on 5/11/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import UIKit
import CoreData

class DetailViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    var product:Product?
    var originalHeight:CGFloat = 0
    var originalRelation =  NSLayoutConstraint()
    var isHidden = false
    var products: [NSManagedObject] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        loadData()
        
        titleLabel.font = UIFont(name: titleLabel.font.fontName, size: titleSize)
        descriptionLabel.font = UIFont(name: descriptionLabel.font.fontName, size: subTitleSize)
        
    }
    
    func loadData(){
        
        titleLabel.text = product?.name
        descriptionLabel.text = product?.description
        
        backendManager.downloadImage(imageURL: (product?.largeImageURL)!) { (image) in
            
            DispatchQueue.main.async {
                
                self.productImage.image = image
            }
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch:UITouch in touches {
            if touch.view?.tag == 1 {
                hideImage()
            } else if isHidden {
                
                UIView.animate(withDuration: 0.5, animations: {
                    self.heightConstraint.constant = self.originalHeight
                    self.view.layoutIfNeeded()
                    self.isHidden = false
                })
            }
        }
    }
    
    func hideImage() {
        
        originalHeight = heightConstraint.constant
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.heightConstraint.constant = 0
            self.view.layoutIfNeeded()
            self.isHidden = true
        })
        
    }
    
    @IBAction func addToFavourites(_ sender: Any) {
        
        backendManager.saveProduct(product: product!)
        
    }
    
    @IBAction func deleteProduct(_ sender: Any) {
        
        backendManager.deleteProduct(product!)
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
