//
//  ProductTableViewCell.swift
//  TestMomentum
//
//  Created by Sebastian Guerrero on 5/10/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {
    
    
    var product:Product?

    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var productTitle: UILabel!
    
    @IBOutlet weak var productSubtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func fillCellData(){
        
        productTitle.text = product?.name
        productSubtitle.text = product?.description
        
        productImage.image = product?.image
        
        // Download only if it hasn't been downloaded yet
        
        if product?.thumbnailImageURL != "n/a" && product?.image == #imageLiteral(resourceName: "missingImage.png") {
            
            backendManager.downloadImage(imageURL: (product?.thumbnailImageURL)!, completionHanddler: { (image) in
                
                self.product?.image = image
                
                DispatchQueue.main.async(execute: {
                    
                    self.productImage.image = self.product?.image
                })
            })
            
        }
        
    }
}
