//
//  Product.swift
//  TestMomentum
//
//  Created by Sebastian Guerrero on 5/10/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import Foundation
import UIKit

class Product {
    var prductId:Int
    var name:String
    var description:String
    var largeImageURL:String
    var thumbnailImageURL:String
    var image:UIImage

    init(productId:Int, name:String, description:String, largeImageURL: String,  thumbnailImageURL:String, image:UIImage) {
        self.prductId = productId
        self.name = name
        self.description = description
        self.largeImageURL = largeImageURL
        self.thumbnailImageURL = thumbnailImageURL
        self.image = image
    }

}
