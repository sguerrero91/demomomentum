//
//  BackendManager.swift
//  TestMomentum
//
//  Created by Sebastian Guerrero on 5/10/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import CoreData

let backendManager:BackendManager = .shared

class BackendManager{
    
    static let shared = BackendManager()
    static var products: [NSManagedObject] = []
    
    // MARK: - Server connections
    
    let wallmartURL = "http://api.walmartlabs.com"
    
    func fetchDataFromAPI(url:String , completionHanddler: @escaping (String, [Product]) -> ()){
        
        let url = wallmartURL + url
        
        Alamofire.request(url).responseJSON { response in
            
            guard let json:[String:Any] = response.result.value as? [String : Any] else {
                return
            }
            
            let nextPage = "\(json["nextPage"]!)"
            
            let items = json["items"] as! NSArray
            
            
            var products = [Product]()
            
            for item in items {
                
                guard let itemDict = item as? [String:Any] else {
                    return
                }
                
                let productId = Int("\(itemDict["itemId"] ?? "0")")!
                
                let defaults = UserDefaults.standard
                let deletedProductsArray = defaults.array(forKey: "deletedProducts")  as? [Int] ?? [Int]()
                
                if deletedProductsArray.contains(productId){
                    
                    continue
                    
                }
                                
                let product = Product(productId: productId, name: "\(itemDict["name"] ?? "n/a")", description: "\(itemDict["shortDescription"] ?? "n/a")", largeImageURL: "\(itemDict["largeImage"] ?? "n/a")", thumbnailImageURL: "\(itemDict["thumbnailImage"] ?? "n/a")", image: #imageLiteral(resourceName: "missingImage.png") )
                
                products += [product]
                
            }
            
            completionHanddler(nextPage, products)
            
        }
        
    }
    
    func downloadImage(imageURL:String, completionHanddler: @escaping (UIImage) -> ()){
        
        
        Alamofire.request(imageURL).responseImage { response in
            
            guard let image = response.result.value else {
                return
            }
            
            completionHanddler(image)
            
        }

        
    }
    
    // MARK: - Core Data
    
    func saveProduct(product:Product){
        
        // Only save if it's not aready saved 
        
        let prdArray = fetchProduct(product)
        
        if prdArray.count > 0 {
            return
        }
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSEntityDescription.entity(forEntityName: "ProductEntity",
                                       in: managedContext)!
        
        let productEntity = NSManagedObject(entity: entity,
                                            insertInto: managedContext)
        
        productEntity.setValue(product.name, forKey: "name")
        productEntity.setValue(product.prductId, forKey: "id")
        productEntity.setValue(product.description, forKey: "desc")
        
        do {
            try managedContext.save()
            BackendManager.products.append(productEntity)
        } catch let error as NSError {
            print("Error: \(error)")
        }
        
    }
    
    func fetchFromCoreData(){
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "ProductEntity")
        
        do {
            BackendManager.products = try managedContext.fetch(fetchRequest) as! [ProductEntity]
        } catch let error as NSError {
            print("Error: \(error)")
        }
        
        for prod in BackendManager.products {
            
            
            let productAux = Product(productId: Int("\(prod.value(forKey: "id")!)")!, name: "\(prod.value(forKey: "name")!)", description: "\(prod.value(forKey: "desc")!)", largeImageURL: "n/a", thumbnailImageURL: "n/a", image: #imageLiteral(resourceName: "missingImage.png"))
            
            productArray += [productAux]
            
        }
        
    }
    
    func deleteProduct(_ product:Product){
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
 
        
        let fetchedProducts = fetchProduct(product)
        
        
        // this should happen only once as we have the restrictions of only saving an object once
        
        for prd in fetchedProducts{
            
            managedContext.delete(prd)
        }
        
        do {
            
            try managedContext.save()
            
            if let index = productArray.index(where: { $0.prductId == product.prductId }) {
                
                productArray.remove(at: index)
                
                // add prodct ID to userdefaults when deleted
                
                let defaults = UserDefaults.standard
                var deletedProductsArray = defaults.array(forKey: "deletedProducts")  as? [Int] ?? [Int]()
                
                deletedProductsArray += [product.prductId]
                defaults.set(deletedProductsArray, forKey: "deletedProducts")
                
            }
            
        } catch let error as NSError {
            print("Error: \(error)")
        }

        
    }
    
    
    
    
    func fetchProduct(_ product:Product) -> [ProductEntity]{
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return []
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let fetchRequest:NSFetchRequest<ProductEntity> = ProductEntity.fetchRequest()
        
        fetchRequest.predicate = NSPredicate(format: "id == %ld", product.prductId)
        
        do {
            
            let fetchedResults = try managedContext.fetch(fetchRequest) as [ProductEntity]
            
            if let _ = fetchedResults.first {
                
                return fetchedResults
                
            } else {
                
                return []
                
            }
            
        } catch let error as NSError {
            print("Error: \(error)")
        }

        return []
    }
    
}
