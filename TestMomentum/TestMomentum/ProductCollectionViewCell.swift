//
//  ProductCollectionViewCell.swift
//  TestMomentum
//
//  Created by Sebastian Guerrero on 5/11/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    
    var product:Product?
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productSubtitle: UILabel!
    
    func fillCellData(){
        
        productTitle.text = product?.name
        productSubtitle.text = product?.description
        productImage.image = product?.image
        
    }
    
}
