//
//  ProductCollectionViewController.swift
//  TestMomentum
//
//  Created by Sebastian Guerrero on 5/11/17.
//  Copyright © 2017 Sebastian Guerrero. All rights reserved.
//

import UIKit

class ProductCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating {

    @IBOutlet weak var prductsCollectionView: UICollectionView!
    
    var selectedIndex = 0
    
    var activeSearch = false
    var filteredProducts = [Product]()
    
    var downloadingNextPage = false
    var rowForPagination = 0
    
    var searchController: UISearchController!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NSNotification.Name("reloadCVData"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if prductsCollectionView.numberOfItems(inSection: 0) == 0 {
            prductsCollectionView.reloadData()
        }
        
    }
    
    
    // MARK: - Config Collection View
    
    func reloadData(){
        
        prductsCollectionView.reloadData()
        
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if activeSearch{
            
            return filteredProducts.count
            
        }
        
        return productArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCVC", for: indexPath) as! ProductCollectionViewCell
        
        cell.productTitle.font = UIFont(name: cell.productTitle.font.fontName, size: titleSize)
        cell.productSubtitle.font = UIFont(name: cell.productSubtitle.font.fontName, size: subTitleSize)
        
        if activeSearch {
            
            cell.product = filteredProducts[indexPath.row]
            
        } else {
            
            cell.product = productArray[indexPath.row]
            
        }
        
        cell.fillCellData()
        
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let productCell = cell as! ProductCollectionViewCell
        
        if productCell.product?.image == #imageLiteral(resourceName: "missingImage.png") {
            
            // Download only if it hasn't been downloaded yet
            
            backendManager.downloadImage(imageURL: (productCell.product?.thumbnailImageURL)!, completionHanddler: { (image) in
                DispatchQueue.main.async(execute: { 
                    productCell.productImage.image = image
                })
            })
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if !downloadingNextPage && productArray.count > 0 && !activeSearch{
            
            if prductsCollectionView.contentOffset.y >= (prductsCollectionView.contentSize.height - prductsCollectionView.frame.size.height) {
                
                downloadingNextPage = true
                rowForPagination = productArray.count - 1
                
                backendManager.fetchDataFromAPI(url: nextPage) { (nextPageS, products) in
                    
                    productArray += products
                    nextPage = nextPageS
                    self.downloadingNextPage = false
                    
                }
            }
            
        }

        
    }
    
    
    @IBAction func orderButtonPressed(_ sender: Any) {
        
        let sortedArray = productArray.sorted(by: { $0.name < $1.name })
        productArray = sortedArray
        rowForPagination = 0
        prductsCollectionView.reloadData()
        
    }
    
    // MARK: - Search controller
    
    func configSerchBarController(){
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionHeader {
            
            configSerchBarController()
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath)
            
            headerView.addSubview(searchController.searchBar)
            return headerView
            
        }
        
        return UICollectionReusableView()
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        activeSearch = true
        prductsCollectionView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        activeSearch = false
        prductsCollectionView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        activeSearch = false
        prductsCollectionView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if !activeSearch {
            
            activeSearch = true
            prductsCollectionView.reloadData()
            
        }
        
        searchController.searchBar.resignFirstResponder()
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        
        filteredProducts = productArray.filter({ (product) -> Bool in
            
            return product.name.lowercased().contains((searchController.searchBar.text?.lowercased())!)
            
        })
        
        prductsCollectionView.reloadData()
        
    }
    
    
    //MARK: - Prepare for segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destination = segue.destination as! DetailViewController
        
        if activeSearch {
            
            destination.product = filteredProducts[(prductsCollectionView.indexPathsForSelectedItems?[0].row)!]
            
        } else {
            
            destination.product = productArray[(prductsCollectionView.indexPathsForSelectedItems?[0].row)!]
            
        }
        searchController.isActive = false        
        
    }
    

}
