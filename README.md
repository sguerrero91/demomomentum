# README #

This project was created to showcase my abilities on Swift

### CocoaPods ###

* Alamofire
* AlamofireImage
* Reachability

### Key features ###

* Implemented reachability, to test it on ProductsTableViewController lines 36 and 37 (one at a time) have to be used if you are using the simulator.
* On DetailViewController, if the image is tapped it will hide, to make it reapear a tap on the screen is needed.
* The button Favourite will add the current product to Core Data, to be retrieved when the device doesn't have an internet connection.
* Use of UserDefaults to store the deleted products id, this will be checked when the products are downloaded from the server
* Pagination, once user has reached the bottom of the tableview or the collection the next 100 (approx it depends on wall mart API) products will be downloaded

###Architecture###

Trying to avoid as much as possible the MVC (massive view controller) architecture, BackendManager manages all the connections with server and CoreData.

To update the view I'm making use of NotificationCenter and an Observer in porductArray that will trigger the notifications when updated.